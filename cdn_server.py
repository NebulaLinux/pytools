#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Name: backup.py
#  Description: Create backup of site.
#  Synopsis:  cdn_manager.py --user user --host host --localfile fileloc --remotefile fileremot
#  Copyright 2017:
#		Sergio López Sanz <slopez.edcom@gmail.com>
#       Ernesto Crespo <ecrespo@gmail.com>
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
#"rsync -avrz /opt/data/filename root@ip:/opt/data/file"
__author__ = 'Ernesto Crespo'
__email__ = "ecrespo@gmail.com"

from sys import argv
from core.filemanager import makedir, cp, cpr, mv, rm, rmr, ln
from core.rsyncmanager import Rsync
import argparse

rootpath= "./tmp/"
temporalpath ="./temporal/"
backuppath = "./pruebas/"


def CaseManager():

    parser = argparse.ArgumentParser(add_help=False,description='CDN tool.')
    parser.add_argument("-u","--user",help="remote user",type=str)
    parser.add_argument("-h","--host",help="host to send file", type=str)
    parser.add_argument("-localfile",help="localfile to sending to another host", type=str)
    parser.add_argument("-remotefile",help="remote path to receive file from CDN server",type=str)


    args = parser.parse_args()
    if args.user == None:
    	raise NameError('Not user to sendfile')
	elif args.host == None:
		raise NameError('Not host to sendfile')
	elif args.localfile == None:
		raise NameError('Not localfile  to send')
	elif args.remotefile == None:
		raise NameError('Not remote path  to receive file')
	user = args.user
	host = args.host
	localfile = args.localfile
	remotefile = args.remotefile
	try:
		rs = Rsync(user,host)
		rs.Push(localfile,remotefile)
	except:
		raise NameError('Can Not send file to remote host')

if __name__ == '__main__':
	CaseManager()

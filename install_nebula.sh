#!/bin/sh

#apt-get update

hostname=""
echo -n 'Insert Hostname for this Server [Blank for default]'
read hostname
if [ -z $hostname ]
then
 echo "no hostname"
else
 echo "host: "$hostname
fi

groupadd chrooted

wget http://nginx.org/keys/nginx_signing.key
apt-key add nginx_signing.key

echo "deb http://nginx.org/packages/mainline/debian/ jessie nginx" >> /etc/apt/sources.list
echo "deb-src http://nginx.org/packages/mainline/debian/ jessie nginx" >> /etc/apt/sources.list

apt-get update

apt-get install -y zip unzip htop mc
apt-get install -y preload prelink
apt-get install -y openssh-server fail2ban
apt-get install -y sqlite3
apt-get install -y php5 php5-fpm php5-sqlite
apt-get install -y nginx
apt-get install -y clamav

./core/replace_Line.py /etc/default/prelink  "PRELINKING=" "PRELINKING=yes"
./core/replace_Line.py /etc/default/prelink  "PRELINK_OPTS=" "PRELINK_OPTS=-amR"

prelink -amR

echo 'DPkg::Post-Invoke {"echo Prelinking......;/etc/cron.daily/prelink";}' >> /etc/apt/apt.conf
                        
wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar

mv wp-cli.phar /usr/local/bin/wp
wp cli update --nightly --allow-root

wget https://dl.eff.org/certbot-auto
chmod a+x certbot-auto
mv certbot-auto /usr/local/bin/certbot-auto
/usr/local/bin/certbot-auto

# Configurar nginx
mkdir /etc/nginx/sites-available
mkdir /etc/nginx/sites-enabled
mkdir /etc/nginx/snippets
mkdir /var/www
mkdir /var/www/html

mv confs/nginx/nginx.conf /etc/nginx
mv confs/fastcgi.conf /etc/nginx
mv confs/fastcgi-php.conf /etc/nginx/snippets

# Instalar pytools
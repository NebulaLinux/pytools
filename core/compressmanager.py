#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  compressmanager.py
#  
#  Copyright 2017:
#		* Sergio López Sanz <slopez.edcom@gmail.com>
#  		* Ernesto Crespo <ecrespo@gmail.com>
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import os
import zipfile
import tarfile
import shutil

class fileZip(object):



	def CompressOneFile(self,fileNameZip,file,compresstype=zipfile.ZIP_DEFLATED):
		fileZip = zipfile.ZipFile(fileNameZip, 'w')
		fileZip.write(file, compress_type=compresstype)
		fileZip.close()

	
	def CompressFiles(self,fileNameZip,directory='./',keepStructure=True,compresstype=zipfile.ZIP_DEFLATED):
		fileZip = zipfile.ZipFile(fileNameZip, 'w')
		for folder, subfolders, files in os.walk(directory):
			if keepStructure == True:
				for file in files:
					fileZip.write(os.path.join(folder, file), os.path.relpath(os.path.join(folder,file), directory), \
						compress_type = compresstype)
			else:
				for file in files:
					fileZip.write(os.path.join(folder, file), file, compress_type = compresstype)
		fileZip.close()


	def UnCompressFiles(self,fileNameZip,directory="./"):
		fileZip = zipfile.ZipFile(fileNameZip)
		fileZip.extractall(directory)
		fileZip.close()



class FileTGZ(object):
	
	def UnCompressFiles(self,fileNameTGZ,directory="./"):
		tar = tarfile.open(fileNameTGZ,"r:gz")
		tar.extractall(directory)
		tar.close()



	def CompressOneFile(self,fileNameTGZ,file):
		tar = tarfile.open(fileNameTGZ, mode='w:gz')
		tar.add(file)
		tar.close()




	def CompressFiles(self,fileNameTGZ,directory='./'):
		tar = tarfile.open(fileNameTGZ, mode='w:gz')
		for folder, subfolders, files in os.walk(directory):
			for file in files:
				tar.add(os.path.join(folder, file))
		tar.close()




class FileCompress(object):

	def __init__(self,fileFormat):
		self.__fileFormat = fileFormat
		if fileFormat == 'zip':
			self.__filezip = fileZip()
		elif fileFormat == 'tgz':
			self.__filetgz = FileTGZ()
		else:
			raise NameError('zip or tgz format only')

		

	def CompressOneFile(self,filename,file):
		if self.__fileFormat == 'zip':
			self.__filezip.CompressOneFile(filename,file)
		elif self.__fileFormat == 'tgz':
			self.__filetgz.CompressOneFile(filename,file)



	def CompressFiles(self,filename,directory="./"):
		if self.__fileFormat == 'zip':
			self.__filezip.CompressFiles(filename,directory)
		elif self.__fileFormat == 'tgz':
			self.__filetgz.CompressFiles(filename,directory)



	def UnCompressFiles(self,filename,directory="./"):
		if self.__fileFormat == 'zip':
			self.__filezip.UnCompressFiles(filename,directory)
		elif self.__fileFormat == 'tgz':
			self.__filetgz.UnCompressFiles(filename,directory)


"""

if __name__ == '__main__':
	
	prueba1 = FileCompress('zip')
	prueba1.UnCompressFiles('/home/ernesto/proyectos/pruebas/prueba1.zip',"/home/ernesto/proyectos/pruebas/tmp1/")
	prueba1 = FileCompress('tgz')
	prueba1.UnCompressFiles('/home/ernesto/proyectos/pruebas/prueba2.tar.gz',"/home/ernesto/proyectos/pruebas/tmp2/")
	prueba1 = FileCompress('zip')
	prueba1.CompressOneFile("/home/ernesto/proyectos/pruebas/prueba4.zip","/home/ernesto/proyectos/pruebas/tmp/prueba.py")
	prueba1 = FileCompress('tgz')
	prueba1.CompressOneFile("/home/ernesto/proyectos/pruebas/prueba5.tar.gz","/home/ernesto/proyectos/pruebas/tmp/prueba.py")
	prueba1 = FileCompress('zip')
	prueba1.CompressFiles("/home/ernesto/proyectos/pruebas/prueba6.zip","/home/ernesto/proyectos/pruebas/tmp/")
	prueba1 = FileCompress('tgz')
	prueba1.CompressFiles("/home/ernesto/proyectos/pruebas/prueba8.tar.gz","/home/ernesto/proyectos/pruebas/tmp/")
	prueba1 = FileCompress('tar')


"""


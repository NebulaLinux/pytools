#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys
from tempfile import mkstemp
from shutil import move
from os import remove, close

def replace(file_path, pattern, subst):
    R=0
    #Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                if pattern in line:
                    new_file.write(subst)
                    R += 1
        if R == 0:
            new_file.write(subst)
        
    close(fh)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)
    
replace(sys.argv[1],sys.argv[2],sys.argv[3])
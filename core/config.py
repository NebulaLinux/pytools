#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  config
#
#  Copyright 2017 Sergio L�pez Sanz <slopez.edcom@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


import ConfigParser, os
from filemanager import rm

def configFilePath():
    return os.path.join(os.path.split(os.path.abspath(__file__))[0],"config.ini")

def rebuildConfFile():
    
    absoluteFilePath = configFilePath()
    
    rm(absoluteFilePath)
    
    cfgfile = open(absoluteFilePath, 'w')
    
    confIni= ConfigParser.ConfigParser()
    confIni.add_section('install')
    confIni.set('install','installDir', "/nebula/")
    confIni.set('install','webDir', "/var/www/html/")
    confIni.set('install','nginx', "/etc/nginx/")
    
    confIni.write(cfgfile)
    
    cfgfile.close()


def getConf(cfgString=""):
    
    config = {}
    
    absoluteFilePath = configFilePath()
    
    confIni= ConfigParser.ConfigParser()
    confIni.read(absoluteFilePath)
    
    try:
        config["installDir"] = confIni.get("install", "installDir")    
        config["webDir"] = confIni.get("install", "webDir")  
    except:
        print "file damaged!\n"
        repair = raw_input("do you want to rewrite the file? [yes/no]: ")
        if repair == "yes":
            rebuildConfFile()
        return "error"

    if cfgString=="":
        return config
    else:
        return config[cfgString]
    
def setConf(config, value):
    
    absoluteFilePath = configFilePath()
    
    confIni = ConfigParser.ConfigParser()
    confIni.read(absoluteFilePath)
    
    confIni.set('install',config, value)

    cfgfile = open(absoluteFilePath, 'w')    
    confIni.write(cfgfile)
    cfgfile.close()

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  dbmysqlmanager.py
#  
#  Copyright 2017:
#          Sergio López Sanz <slopez.edcom@gmail.com>
#          Ernesto Crespo <ecrespo@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
#mysqldump --quick -h 127.0.0.1 -u admin -p 123456 Database > respaldo.sql
#mysql -u “usuario” -p”contraseña” -h”nombre-o-dirección-del-host” nombre-de-la-base-de-datos < nombre-del-respaldo.sql


import datetime
import os

class DBManager(object):
	def __init__(self,host,user,password):
		self.__host = host  
		self.__user = user 
		self.__password = password
		self.__dumpCommand = "mysqldump --quick "
		self.__restoreCommand = "mysql "



	def dump(self,database,filebackupdb):
		self.__database = database
		self.__filebackup = filebackupdb
		self.__dumpCommand = self.__dumpCommand + "-h {0} -u {0} -p {0} {0} > {0}".format(self.__host,self.__user,/
			self.__password,database,filebackupdb)
		os.system(self.__dumpCommand)


	def restore(self,database,filebackupdb):
		self.__database = database
		self.__filebackup = filebackupdb
		self.__restoreCommand = self.__restoreCommand + "-u {0} -p {0} -h {0}  {0} < {0}".format(self.__user,/
			self.__password,self.__host,self.__database,self.__filebackup)
		os.system(self.__restoreCommand)



#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  filemanager.py
#  
#  Copyright 2016 Sergio López Sanz <slopez.edcom@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import sqlite3


def getDBcursor():

    from filemanager import getConfigPath
    conn = sqlite3.connect(getConfigPath()+"userdb" )
    return conn, conn.cursor()

def select(cursor, table):
    pass
    
        

def getAllUsers():
    cursor = getDBcursor()[1]
    data = {}
    for row in cursor.execute("SELECT * FROM users"):
        data.update({row[0]:row[1:]})
    if len(data)==0: 
        return False  
    return data

def getUser(username):
    cursor = getDBcursor()[1]
    data = {}
    for row in cursor.execute("SELECT * FROM users WHERE user=?",(username,)):
        data = {'user':row[0], 'server':row[1], 'virtualserver':row[2]==1}
        
    if len(data)==0: 
        return False  
    return data

def addUser(username, server, virtualserver):
    conn,cursor = getDBcursor()
    if virtualserver:
        virtualserver = 1
    else:
        virtualserver = 0
    if getUser(username) == False:
        cursor.execute("INSERT INTO users VALUES (?,?,?)",(username, server, virtualserver))
        conn.commit()
        return getUser(username)
    else:
        return False

def deleteUser(username):
    conn,cursor = getDBcursor()
    if getUser(username) != False:
        cursor.execute("DELETE FROM users WHERE user=?",(username,))
        conn.commit()
        return True
    else:
        return False
    
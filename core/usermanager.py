#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  usermanager.py
#  
#  Copyright 2017 Sergio López Sanz <slopez.edcom@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from os import system




def createUser(username,password):
    from crypt import crypt
    if not userExists(username):
        encPass = crypt(password,"22")   
        return system("useradd -p "+encPass+ " -s "+ "/bin/bash "+ "-d "+ "/home/" + username+ " -m "+ " -c \""+ username+"\" " + username + " -g chrooted")
    else:
        return "user already exists"

# set the necessary config for openssh-server sftp. We love you stackoverflow 
def setUserSftpConfig(username):
    userDir = "/home/" + username + "/" 
    if not userExists(username):
        return False
    
    system("chown root " + userDir)
    system("chmod go-w " + userDir)
    system("chown " + username + ":chrooted " + userDir + "/*")
    system("chmod ug+rwX " + userDir + "/*")
    
    return True

def addVirtualServerToUser(username, virtualserver):
    from filemanager import append
    from config import getConf
    append(getConf("installDir") + "users", username + ":" + virtualserver)

def getPassword():
    from getpass import getpass
    from sys import stderr
    password1 = getpass(stream=stderr)
    password2 = getpass(stream=stderr, prompt='Retype:')
    if password1 == password2:
        return password1
    else:
        return 0 

def userExists(username):
    output = system("id "+username)
    if output.find("uid") == -1:
        return False
    
    return True

def getUserIds(username):
    from pwd import getpwnam 
    return getpwnam(username).pw_uid, getpwnam(username).pw_gid

def isRoot():
    from os import geteuid
    if geteuid != 0:
        return False
    return True

    
    
    
#!/usr/bin/env pyhton
# -*- coding: utf-8 -*-
#
#  rsyncmanager.py
#
#  Copyright 2016:
#        Sergio López Sanz <slopez.edcom@gmail.com>
#        Ernesto Crespo <ecrespo@gmail.com>
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
#os.system("rsync -avrz /opt/data/filename root@ip:/opt/data/file")
import os

def RSync(object):
    def __init__(self, user,host):
        self.__user = user
        self.__host = host


    @property
    def user(self):
        return self.__user

    @user.setter
    def user(self,user):
        self.__user = user

    @property
    def host(self):
        return self.__host

    @host.setter
    def host(self,host):
        self.__host = host

    def Push(localfile,remotepathfile):
        command = "rsync -avrz {0} {0}@{0}:{0}".format(localfile,self.__user,self.__host,remotepathfile)
        os.system(command)

        

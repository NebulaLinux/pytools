#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  filemanager.py
#
#  Copyright 2016 Sergio López Sanz <slopez.edcom@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#



import errno

def makedir(path):
    from os import mkdir
    try:
        mkdir (path)
    except OSError as e:
        if e.errno == errno.EACCES:
            return "not permitted"
    return True

def cp(source, dest):
    from shutil import copy
    try:
        copy(source, dest)
    except OSError as e:
        if e.errno == errno.EACCES:
            return "not permitted"
    return True

def cpr(source, dest):
    from shutil import copytree
    try:
        copytree(source, dest)
    except Exception as e: print str(e)

def mv(source, dest):
    from shutil import move
    try:
        move(source, dest)
    except OSError as e:
        if e.errno == errno.EACCES:
            return "not permitted"
    return True

def ln(source, dest):
    from os import symlink
    try:
        symlink(source, dest)
    except:
        pass
def rm(source):
    from os import remove
    try:
        remove(source)
    except OSError as e:
        if e.errno == errno.EACCES:
            return "not permitted"
    return True

def rmr(source): # remove tree
    from shutil import rmtree
    try:
        rmtree(source)
    except Exception as e: print str(e)


def append(source, line):
    with open(source, 'a') as file:
        file.write(line + "\n")

def getConfigPath():
    from os import path, sep
    di = path.dirname(path.realpath(__file__))
    return di[0:di.rfind(sep)]+sep

def getOwner(path):
    from os import stat
    from pwd import getpwuid
    return getpwuid(stat(path).st_uid).pw_name

def setOwner(path, user, group):
    from os import chown
    from core.usermanager import getUserIds
    try:
        chown(path, getUserIds(user)[0], getUserIds(group)[1])
    except OSError as e:
        if e.errno == errno.EACCES:
            return "not permitted"
    return True

def setOwnerTree(basePath, user, group):
    from os import walk , path

    setOwner(basePath)
    for root, dirs, files in walk(basePath):
        for di in dirs:
            setOwner(path.join(root, di), user, group)
        for fi in files:
            setOwner(path.join(root, fi), user, group)


def checkfile(file):
    import os.path
    if os.path.exists(file):
        return True
    else:
        return False

- Conjunto de scripts necesarios para crear una infraestructura de cdn, con un servidor maestro (que puede ser el mismo que el servidor web u otro distinto), y servidores nodo:

El usuario sube un archivo al servidor maestro, el servidor dispone de una lista de nodos (editable), el srv maestro envía el archivo a los servidores incluidos en la lista de nodos.

Cuando el nodo recibe el archivo (posiblemente cifrado y vía sftp) lo coloca en un directorio web, y envía de vuelta su url (por ejemplo cdn1.miweb.com/pdf/loquesea.pdf) al servidor maestro. Se almacenan y gestionan todas las url's para poder utilizarlas correctamente en las webs.




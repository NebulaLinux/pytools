#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Name: backup.py
#  Description: Create backup of site.
#  Synopsis:  backup.py -zip|-tgz -sitename <sitename> -check
#  Copyright 2017: 
#		Sergio López Sanz <slopez.edcom@gmail.com>
#       Ernesto Crespo <ecrespo@gmail.com>  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

"""
Synopsis:
backup.py -create|-extract  -zip|-tgz -gen|-check  -sitename <namesite> -db mysql|sqlite  -mysql-user user -mysql-password password -mysql-host host   

Create backup:
backup.py -create -tgz -gen -sitename nebulaLinux -db sqlite  -db-file slqlitefile  file
backup.py -create -zip -gen -sitename nebulaLinux -db mysql -mysql-user nebulauser -mysql-password 123456  -mysql-host nebulaserver file

check backup:

backup.py -check filesite

Extract backup:

backup.py -extract -zip -sitename nebulaLinux -db sqlite  backup-nebulaLinux-dateandtime-md5hash.zip
backup.py -extract -tgz -sitename nebulaLinux -db mysql  -mysql-user nebulauser -mysql-password 123456  -mysql-host nebulaserver   backup-nebulaLinux-dateandtime-md5hash.tar.gz 

"""

__author__ = 'Ernesto Crespo'
__email__ = "ecrespo@gmail.com"

from sys import argv
from core.filemanager import makedir, cp, cpr, mv, rm, rmr, ln
from core.config import getConf
import argparse
from core.compressmanager import FileCompress
from core.dbmysqlmanager import DBManager
import hashlib

rootpath= "./tmp/"
temporalpath ="./temporal/"
backuppath = "./pruebas/"


def hashcheck(filename):
	hasher = hashlib.md5()
    	with open(filename, 'rb') as afile:
    		buf = afile.read()
    		hasher.update(buf)
    		return hasher.hexdigest()

def CaseManager():
    
    parser = argparse.ArgumentParser(add_help=False,description='backup website.')
    parser.add_argument("file",help="filename to backup",type=str)
    parser.add_argument("-o","--options",help="option to backup",choices=['check', 'create', 'extract'])
    parser.add_argument("-format",help="format option zip or .tar.gz", choices=["zip","tgz"])
    parser.add_argument("-db",help="database type sqlite or mysql", choices=["sqlite","mysql"])
    parser.add_argument("-dbpath",help="path for sqlite file",type=str)
    parser.add_argument("-confpath",help="path for config file website",type=str)
    parser.add_argument("-websitepath",help="path for website",type=str)
    parser.add_argument("-mysqluser",help="",type=str)
    parser.add_argument("-mysqlpassword",help="",type=str)
    parser.add_argument("-mysqlhost",help="",type=str)
    parser.add_argument("-mysqldb",help="",type=str)


    args = parser.parse_args()
    if args.file == None:
    	raise NameError('Not file argument')
    filename = args.file 
    if args.confpath == None:
    	raise NameError('Not confpath argument')
    if args.websitepath == None: 
    	raise NameError('Not websitepath argument')
    confpath = args.confpath
    websitepath= args.websitepath
    if args.options == "check":
    	if (args.format != None) or (args.format != None) or (args.db != None) or (args.mysqluser != None) or (args.mysqlpassword != None) or (args.mysqlhost != None):
    		raise NameError('Not need')
    	print (hashcheck(filename))
    elif args.options == "create" or args.options == "extract":
    	
    	if args.format != None:
    		formatfile = args.format
    	else: 
    		raise NameError('zip or tgz format only')
    	if args.db != None:
    		databasetype = args.db
    	else:
    		raise NameError('sqlite or mysql database only')
    	
    	if databasetype == "sqlite" and args.dbpath == None :
    		raise NameError('sqlite file path not exist')
    	elif databasetype == "mysql" and args.dbpath == None:
    		raise NameError('mysql file backup/restore path not exist')
    	else:
    		dbpath = args.dbpath
    	if args.options == "extract" and databasetype == "sqlite":
    		filecompress = FileCompress(formatfile)
    		filecompress.UnCompressFiles(filename,temporalpath)
    		absconfpath1 = temporalpath + confpath
    		absconfpath2 = rootpath + confpath
    		mv(absconfpath1,absconfpath2)
    		abswebsitepath1 = temporalpath + websitepath
    		abswebsitepath2 = rootpath * websitepath
    		mv(abswebsitepath1,abswebsitepath2)
    		absdbpath1 = temporalpath + dbpath
    		absdbpath2 = rootpath + dbpath 
    		mv(absdbpath1,absdbpath2)
    		rmr(temporalpath)
    		return True
    	elif args.options == "create" and databasetype == "sqlite":
    		cpr(confpath,temporalpath)
    		cp(dbpath,temporalpath)
    		cpr(websitepath,temporalpath)
    		filecompress = FileCompress(formatfile)
    		datetime = time.strftime("%d%m%Y%H%M%S")
    		if formatfile == "tgz":
    			extensionformat = ".tar.gz"
    		elif formatfile = "zip":
    			extensionformat = ".zip"
    		backupfile = backuppath + filename +"-"+ datetime 
    		backupfileext = backupfile + extensionformat
    		filecompress(backupfileext,temporalpath)
    		signfile = hashcheck(backupfileext)
    		backupfileextend = backupfile + "-" + extensionformat
    		mv(backupfileext,backupfileextend)
    		rmr(temporalpath)
    		return True

    	elif databasetype == "mysql":
    		if (args.mysqluser == None) or (args.mysqlpassword == None) or (args.mysqlhost == None):
    			raise NameError('Not mysql data to connect')
    		if args.mysqldb == None:
    			raise NameError('Not mysql db name argument')
    		mysqldb = args.mysqldb
    		mysqluser = args.mysqluser
    		mysqlhost = args.mysqlhost
    		mysqlpassword = args.mysqlpassword
    		dbmanager = DBManage(mysqlhost,mysqluser,mysqlpassword)
    		
    		if args.options == "create":
    			cpr(confpath,temporalpath)
    			dbmanager.dump(mysqldb,dbpath)
    			cp(dbpath,temporalpath)
    			cpr(websitepath,temporalpath)
    			filecompress = FileCompress(formatfile)
    			datetime = time.strftime("%d%m%Y%H%M%S")
    			if formatfile == "tgz":
    				extensionformat = ".tar.gz"
    			elif formatfile = "zip":
    				extensionformat = ".zip"
    			backupfile = backuppath + filename +"-"+ datetime
    			backupfileext = backupfile + extensionformat
    			filecompress(backupfileext,temporalpath)
    			signfile = hashcheck(backupfileext)
    			backupfileextend = backupfile + "-" + extensionformat
    			mv(backupfileext,backupfileextend)
    			rmr(temporalpath)
    			return True
    		elif args.options == "extract":
    			filecompress = FileCompress(formatfile)
    			filecompress.UnCompressFiles(filename,temporalpath)
    			absconfpath1 = temporalpath + confpath
    			absconfpath2 = rootpath + confpath
    			mv(absconfpath1,absconfpath2)
    			abswebsitepath1 = temporalpath + websitepath
    			abswebsitepath2 = rootpath * websitepath
    			mv(abswebsitepath1,abswebsitepath2)
    			absdbpath1 = temporalpath + dbpath
    			absdbpath2 = rootpath + dbpath
    			mv(absdbpath1,absdbpath2)
    			dbmanager.restore(mysqldb,absdbpath2)
    			rmr(temporalpath)
    			return True





if __name__ == '__main__':
	CaseManager()
	